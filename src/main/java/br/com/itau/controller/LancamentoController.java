package br.com.itau.controller;

import br.com.itau.model.Lancamento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class LancamentoController {
    @Autowired
    Lancamento lancamento;

    @GetMapping("/lancamentos")
    public List<Lancamento> listaLancamentos(){
        return lancamento.listaLancamentos();
    }

    @GetMapping("/lancamentos/{id}")
    public Optional<Lancamento> lancamentoId(@PathVariable(value = "id")int id) {
        return lancamento.lancamentoId(id);
    }

    @GetMapping("/lancamentos/categoria/{categoria}")
    public List<Lancamento> lancamentoCategoria(@PathVariable(value = "categoria")int categoria) {
        return lancamento.lancamentoCategoria(categoria);
    }



}
