package br.com.itau.controller;

import br.com.itau.model.Categoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CategoriaController {
    @Autowired
    private Categoria categoria;


    @GetMapping("/categorias")
    public List<Categoria> listaCategorias(){
        return categoria.listaCategorias();
    }

    @GetMapping("/categorias/{id}")
    public Optional<Categoria> categoriaId(@PathVariable(value = "id")int id) {
        return  categoria.categoriaId(id);

    }


}
