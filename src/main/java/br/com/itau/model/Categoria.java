package br.com.itau.model;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

//import javax.persistence.Entity;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


//@Entity
@Service
public class Categoria {
    private int id;
    private String nome;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Categoria{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                '}';
    }
    //RestTemplate
    RestTemplate restCategoria = new RestTemplateBuilder()
            .rootUri("https://desafio-it-server.herokuapp.com/categorias")
            .build();


    public List<Categoria> listaCategorias(){
        ResponseEntity<Categoria[]> responses =
                restCategoria.getForEntity("https://desafio-it-server.herokuapp.com/categorias", Categoria[].class );
        List<Categoria> list = Arrays.asList(responses.getBody());
        return list;

    }

    public Optional<Categoria> categoriaId(int id){
        Optional<Categoria> resultado = Optional.ofNullable(listaCategorias().stream()
                .filter(x -> id == x.getId())
                .findAny()
                .orElse(null));
        return resultado;
    }


}

