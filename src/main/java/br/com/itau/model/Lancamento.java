package br.com.itau.model;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

//import javax.persistence.Entity;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

//@Entity
@Service
public class Lancamento {
    private int id;
    private Double valor;
    private String origem;
    private int categoria;
    private int mes_lancamento;


    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }


    public int getMes_lancamento() {
        return mes_lancamento;
    }

    public void setMes_lancamento(int mes_lancamento) {
        this.mes_lancamento = mes_lancamento;
    }

    @Override
    public String toString() {
        return "Lancamento{" +
                "id=" + id +
                ", valor=" + valor +
                ", origem='" + origem + '\'' +
                ", categoria=" + categoria +
                ", mes_lancamento=" + mes_lancamento +
                '}';
    }

    //RestTemplate
    RestTemplate restLancamento = new RestTemplateBuilder()
            .rootUri("https://desafio-it-server.herokuapp.com/categorias")
            .build();

    public List<Lancamento> listaLancamentos() {
        ResponseEntity<Lancamento[]> responses =
                restLancamento.getForEntity("https://desafio-it-server.herokuapp.com/lancamentos", Lancamento[].class);
        List<Lancamento> list = Arrays.asList(responses.getBody());
        return list;
    }

    public Optional<Lancamento> lancamentoId(int id){
        Optional<Lancamento> resultado = Optional.ofNullable(listaLancamentos().stream()
                .filter(x -> id == x.getId())
                .findAny()
                .orElse(null));
        return resultado;
    }

    public List<Lancamento> lancamentoCategoria(int categoria){
       List<Lancamento> resultado = (List<Lancamento>) listaLancamentos().stream()
                .filter(x -> categoria == x.getCategoria())
                .collect(Collectors.toList());
       return resultado;

    }

}

